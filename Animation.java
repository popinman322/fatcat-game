import greenfoot.*;

/**
 * A class which summerizes animation behavior.
 * 
 * @author Jean-Claude Paquin
 */
public class Animation  
{
    /**
     * The default interval between animations, in "acts".
     */
    public static final int DEFAULT_INTERVAL = 2;
    
    private GreenfootImage[] frames;
    private int interval, currentAct = 0;
    private int currentFrame = 0;
    
    /**
     * Provides a convenience method for creating an animation.
     * args - a list of filenames
     */
    public Animation(String... args) {
        this(DEFAULT_INTERVAL, args);
    }
    
    /**
     * interval - the number of acts it takes to update the animation
     * args - a list of filenames
     */
    public Animation(int interval, String... args) {
        frames = new GreenfootImage[args.length];
        for(int index = 0; index < args.length; index++) {
            frames[index] = new GreenfootImage(args[index]);
        }
        
        this.interval = interval;
    }
    
    /**
     * Provides the image we're currently working with.
     */
    public GreenfootImage getFrame() {
        if(currentFrame == frames.length) {
            currentFrame = 0;
        }
        return frames[currentFrame];
    }
    
    /**
     * Increments the act count and sets the image accordingly.
     */
    public void update(Actor a) {
        if(++currentAct > interval) {
            currentAct = 0;
            currentFrame++;
            a.setImage(getFrame());
        }
    }
}
