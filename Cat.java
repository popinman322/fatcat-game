import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.concurrent.atomic.*;

/**
 * Write a description of class Catcher here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Cat  extends Actor
{
    // Conserving resources... even though there's only one cat :x
    private static GreenfootImage eat_pizza = new GreenfootImage("cat-eat.png"),
                                  eat_pizza2 = new GreenfootImage("cat-eat2.png"),
                                  cat_sit = new GreenfootImage("cat-sit.png");
    // More than one cat, can't conserve :c
    private Animation walkRight = new Animation(7, "cat-walk-right.png", "cat-walk-right2.png"),
                      walkLeft  = new Animation(7, "cat-walk-left.png", "cat-walk-left2.png");
    private int direction = 0; // The direction delta
    
    /*
     * It is excruciatingly important that this
     * variable is volatile! If it is not, there
     * is no guarantee it will be read properly!!!
     */
    private volatile boolean eating = false;
    
    // Black magic ahead...
    // Facilitates synchronization so I don't screw with greenfoot
    private Object sync;
     
     /**
     * Act - do whatever the Catcher wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    @Override
    public void act() 
    {
        // We can only act if we're not eating.
        if(!eating) {
            handleMovement();
            eat(Pizza.class);
        }
    }    
    
    /**
     * Returns 10 when shift is down (sprinting)
     * Returns 5 when shift is up (walking)
     */
    public int getMovespeed() {
        return Greenfoot.isKeyDown("shift") ? 10 : 5;
    }
    
    /**
     * Handles movement and movement related animations.
     */
    public void handleMovement() {
        direction = (Greenfoot.isKeyDown("left") ? -1 : 0) +
                    (Greenfoot.isKeyDown("right") ? 1 : 0);
        switch(direction) {
            case 0: // Sitting
                setImage(cat_sit);
                break;
            case -1: // Walking left
                walkLeft.update(this);
                break;
            case 1: // Walking right
                walkRight.update(this);
                break;
        }
        move(direction*getMovespeed());
    }

     /**
     * Try to delete an object of class 'clss'. This is only successful if there
     * is such an object where we currently are. Otherwise this method does
     * nothing. Returns false if nothing was deleted, returns true if something
     * was deleted
     */
    public void eat(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        if(actor != null) {
            eating = true;
            ((CatWorld)getWorld()).onEaten();
            getWorld().removeObject(actor);  //remove pizza from world
            
            //do "animation"
            // CAUTION: Done in separate thread!
            playEatAnim();
        }
    }
    
    /**
     * Provides a synchronized version of the default setImage method.
     */
    @Override
    public void setImage(GreenfootImage image) {
        // Don't waste resources with a useless set.
        if(getImage() != null && getImage().equals(image))
            return;
        
        /*
         * Lazy initialization is used because Greenfoot uses
         * setImage before the constructor is called.
         * Don't ask me how or why, it just does.
         */
        if(sync == null)
            sync = new Object();
        
        /*
         * Synchronize on object sync, not this Cat object.
         * Important because other objects might synchronize using this Cat,
         * leading to boatloads of concurrency issues.
         * Long story short: We're safe... for now.
         */
        synchronized(sync) {
            super.setImage(image);
        }
    }
    
    /**
     * Check if pizza is near the cat in a radius.
     */
    public boolean canSee(Class clss)
    {
        Actor actor = getOneObjectAtOffset(0, 0, clss);
        return actor != null;        
    }
    
    /**
     * Waits for a given amount of time.
     * @see greenfoot.Greenfoot#delay(int)
     */
    public void wait(int time)
    {
        Greenfoot.delay(time);
    }
    
    /**
     * Provides a "safe" thread sleeping method.
     * By safe I mean "doesn't propagate exceptions".
     */
    private void safeSleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Uses black magic to play the eating animation without pausing the entire game.
     * This allows pizzas to fall while the cat is eating (like they would in real life).
     */
    private void playEatAnim() {
        new Thread() {
            public void run() {
                setImage(eat_pizza);
                safeSleep(100);
                setImage(eat_pizza2);
                safeSleep(100);
                setImage(cat_sit);
                eating = false;
            }
        }.start();
    }
}
