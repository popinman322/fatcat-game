import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FallWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CatWorld  extends World
{
    public final int GAME_LENGTH = 10; // In seconds
    private final long LENGTH_IN_MILLIS = GAME_LENGTH*1000;
    
    // Expecting at least 200ms of error
    private long endTime = System.currentTimeMillis()+LENGTH_IN_MILLIS+200;
    private int secondsLeft = GAME_LENGTH;
    
    private Cat player;
    private int eaten = 0;
    
    /**
     * Constructor for objects of class FallWorld.
     * 
     */
    public CatWorld()
    {    
        // Create a new world with 20x20 cells with a cell size of 10x10 pixels.
        super(700, 500, 1); 
        //add a cat on position
        player = new Cat();
        addObject(new Cat(), getWidth()/2, getHeight()-(player.getImage().getHeight()/2));
    }
    
    /**
    * act - things you want to do each time around go in here
    * 
    */
    public void act()
    {
        if(System.currentTimeMillis() >= endTime) {
            Greenfoot.stop();
            System.out.println("Game over!");
            System.out.printf("You ate %d pizza(s)!\n", eaten);
            return;
        } else {
            int secsLeft = (int)((endTime-System.currentTimeMillis())/1000L);
            if(secsLeft < secondsLeft && secsLeft > 0) {
                System.out.printf("%d seconds remaining.\n", secsLeft);
                secondsLeft = secsLeft;
            }
        }
        
        if(Greenfoot.getRandomNumber(100)<2)
            addObject(new Pizza(), Greenfoot.getRandomNumber(getWidth()), 0);
    }
    
    /**
     * Route back through the world because Greenfoot has concurrency issues.
     */
    public void onEaten() {
        eaten++;
    }
}
